# Phaser 101 Introduction to Game Development

![ScreenShot](2Dgame.png)

Phaser 3 is the game development framework which has taken the developer community by storm. Discover how to make the most of this JavaScript library while building a game that features simple and intuitive mechanics. 

### Summary

Basic Requirements:
1. Web browser
2. Code Editor
3. Web server or use Visual Studio Code Live Server add-ons

##### Code Editors pick one

- Beackets (https://brackets.io)
- Atom (https://atom.io)
- Sublime Text (https://www.sublimetext.com/)
- Visual Studio Code (https://code.visualstudio.com/)

##### Web server -http-server
1. Download and install Node.js (https://nodejs.org)
2. (Windows only) install Git Bash (https://gitforwindows.org/)
3. Open terminal, install http-server: npm install http-server -g   or use Visual Studio Core live-server add-ons
4. Navigate to the project folder, run the server: http-server      or use Visual Studio Core live-server add-ons

#### Rendering Sprites

When you download images GameScene this live sycle is:
init() -> preload() -> create() -> update()

- init(): is once

- preload(): phaser load all images and audio files to the memory this time

- create(): Run only once when preload phase is finished. That time images loaded screen

When you call create method to draw image in web-site you have notive x,y-axis cordinates.

You have to remember change the origin to the top left corner

- update(): Call each frame

##### Scaling and Flipping

Order how you call images is very important. First bavkground then other. Or you have give depth e.g. then 
image is top or bottom

``` 
player.depth = 1;
```
##### Rotation and Update

Rotating sprite 

```
let dragon1 = this.add.sprite(250,180, 'dragon');
dragon1.setScale(3); // 3 x size
dragon1.setAngle(45); // 45 degree
```