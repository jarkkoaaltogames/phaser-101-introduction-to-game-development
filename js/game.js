// Create a new scene
let gameScene = new Phaser.Scene('Game');

// Load assets nothing happened
gameScene.preload = function(){
    // load images
    this.load.image('background','/assets/background.png');
    this.load.image('player','/assets/player.png');
    this.load.image('dragon','/assets/dragon.png');
    this.load.image('treasure','/assets/treasure.png');
    
};

/*
Challenge 01: Place the sprite in the middle of the screen by changing the position
- Don't change the origin
- Use the setPosition method
*/
gameScene.create = function() {
    // Create background sprite
    let bg = this.add.sprite(0, 0, 'background');
    

    // change the origin to the top left corner
    bg.setOrigin(0,0);

    // challenge 01: place sprite in the center
    // bg.setPosition(640/2, 360/2);
    // or
    // let gameW = this.sys.game.config.width;
    // let gameH = this.sys.game.config.height;
    // IF YOU WANT MORE INFORMATION gameheight, gamewidth or bg or scene
    // console.log(gameW, gameH);
    // console.log(bg);
    // console.log(this.scene);

    // Create a player
    this.player = this.add.sprite(50,180,'player');

    // we are reduciing the width and height 0.5
    this.player.setScale(0.5);
    console.log(this.player);

    // Create an enemy and scaling 
    this.dragon = this.add.sprite(250,180,'dragon');
    // this.dragon.scaleX = 1.5;
    // this.dragon.acaleY = 1.5;

    // Create a  second and third enemy and more
    this.dragon1 = this.add.sprite(300,180,'dragon');
    this.dragon2 = this.add.sprite(200,150,'dragon');
    this.dragon3 = this.add.sprite(400,300,'dragon');
    this.dragon4 = this.add.sprite(430,100,'dragon');

    // Flip object
    // this.dragon1.flipX = true;

};

/*
Challenge 02:
- Make a sprite grow over time only until it reaches twice it's original dimensions (width and height)
*/

// Update this is called up to 60 times per second
gameScene.update = function(){
  this.dragon.X += 0.2;
  this.dragon1.X += 0.2;
  this.dragon2.angle += 0.5;
  this.dragon3.Y += 0.1;
  this.dragon4.Y -= 0.1; 

  // Scehect if we're reached scale of 2
  if(this.player.scaleX < 1.2){
  // Make the player grow
  this.player.scaleX += 0.001;
  this.player.scaleY += 0.001;
}
};


// set the configuration of the game
let config = {
    type: Phaser.AUTO, // phaser will use WebGL if available if not it will use canvas
    width: 640,
    height: 360,
    scene: gameScene
};

// create a new game, pass the configuration
let game = new Phaser.Game(config);